// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_SixParams(FOnHealthChangedSignature, UHealthComponent*, HealthComp, float, Health, float, HealtDelta, const class UDamageType*, DamageType, class AController*, InstigatedBy, AActor*, DamageCauser);

UCLASS( ClassGroup=(MultiplayerGame), meta=(BlueprintSpawnableComponent) )
class UE_GAME_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UHealthComponent();

protected:
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

	/**/
	UPROPERTY(Replicated, BlueprintReadOnly, Category = Health)
	float HealthPoint;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Health)
	float DefaultHealthPoint;

	//************************************
	// Met�dus:			OnHealthChange
	// Teljes n�v:		AFPSCharacter::OnHealthChange
	// El�rhet�s�g:	    protected 
	// Visszat�r�s:		void
	// M�dost�:			-
	// Param�terek:		UHealthComponent* HealthComp		- �let komponens
	//					float Health						- �let			
	//					const class UDamageType* DamageType	- sebz�s t�pusa
	//					class AController* InstigatedBy		- ki szenvedi el
	//					AActor* DamageCauser				- ki sebezte
	// Ler�s:			Ez egy figyel� met�dus ami menden olyan esem�nyre reag�l ami az �let v�ltoz�s�val j�r
	//************************************
	UFUNCTION()
	void HandleTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

public:
	UPROPERTY(BlueprintAssignable, Category = Events)
	FOnHealthChangedSignature OnHealthChange;
};

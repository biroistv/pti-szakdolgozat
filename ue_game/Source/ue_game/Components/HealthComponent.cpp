// Fill out your copyright notice in the Description page of Project Settings.

#include "HealthComponent.h"

#include "Net/UnrealNetwork.h"

#include "GameFramework/Actor.h"


// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	this->HealthPoint = 100;	// alapértelmezett élet

	SetIsReplicated(true);
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// Csak akkor elérhető ha mi vagyunk a szever
	
	if (GetOwner()->Role == ROLE_Authority)
	{
		AActor* Actor = GetOwner();
		if (Actor == nullptr) return;

		Actor->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::HandleTakeAnyDamage);
	}

	this->DefaultHealthPoint = this->HealthPoint;
}

void UHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UHealthComponent, HealthPoint);
}

void UHealthComponent::HandleTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (Damage <= 0.f) return;

	this->HealthPoint = FMath::Clamp(this->HealthPoint - Damage, 0.f, this->DefaultHealthPoint);

	OnHealthChange.Broadcast(
		this,
		this->HealthPoint,
		Damage,
		DamageType,
		InstigatedBy,
		DamageCauser);
}


// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSGame_GameMode.h"

#include "Engine/World.h"
#include "EngineUtils.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Actor.h"
#include "GameFramework/PlayerStart.h"

#include "TimerManager.h"

#define OUT


AFPSGame_GameMode::AFPSGame_GameMode()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AFPSGame_GameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void AFPSGame_GameMode::BeginPlay()
{
	Super::BeginPlay();

	GetWorldTimerManager().SetTimer(
		this->TimeHandle,
		this,
		&AFPSGame_GameMode::RespawnPlayer,
		this->RespawnTimer,
		true,
		0.f);
}

void AFPSGame_GameMode::RespawnPlayer()
{
	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		APlayerController* PlayerController = It->Get();

		if (PlayerController && PlayerController->GetPawn() == nullptr)
		{
			RestartPlayerAtPlayerStart(PlayerController, ChoosePlayerStart(PlayerController)); 
		}
	}
}

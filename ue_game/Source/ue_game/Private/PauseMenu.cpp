// Fill out your copyright notice in the Description page of Project Settings.

#include "PauseMenu.h"


bool UPauseMenu::Initialize()
{
	bool bInitSuccess = Super::Initialize();
	if (!bInitSuccess) return false;


	if (this->BackToGame == nullptr) return false;
	this->BackToGame->OnClicked.AddDynamic(this, &UPauseMenu::BackToTheGame);

	if (this->ExitToMainMenu == nullptr) return false;
	this->ExitToMainMenu->OnClicked.AddDynamic(this, &UPauseMenu::QuitToMainMenu);

	return true;
}

void UPauseMenu::BackToTheGame()
{
	if (this->MenuInterfce == nullptr) return;
	this->MenuInterfce->BackToTheGame();
}

void UPauseMenu::QuitToMainMenu()
{
	if (this->MenuInterfce == nullptr) return;
	this->MenuInterfce->QuitToMainMenu();
}
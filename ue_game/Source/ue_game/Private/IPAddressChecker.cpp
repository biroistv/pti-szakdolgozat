// Fill out your copyright notice in the Description page of Project Settings.

#include "IPAddressChecker.h"
#include "Internationalization/Regex.h"

bool IPAddressChecker::IsValidIPAddress(const FString IPAddress)
{
	if (IPAddress.IsEmpty()) return false;

	const FRegexPattern Regexpattern{
		"\\b(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\b$"};

	if (FRegexMatcher::FRegexMatcher(Regexpattern, IPAddress).FindNext())
	{
		return true;
	}

	return false;
}

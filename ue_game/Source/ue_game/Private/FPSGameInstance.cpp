// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSGameInstance.h"

#include "Engine/Engine.h"
#include "Engine/World.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "GameFramework/PlayerController.h"
#include "MenuSystem/MainMenu.h"
#include "MenuSystem/MenuWidget.h"
#include "GenericPlatform/GenericPlatformMisc.h"
#include "Blueprint/UserWidget.h"

UFPSGameInstance::UFPSGameInstance(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer)
{
	/* 
		A blueprint oszt�lyok megkeres�se 
	*/
	static ConstructorHelpers::FClassFinder<UUserWidget> MainMenuBPClass(TEXT("/Game/data/menu_system/wbp_main_menu"));
	static ConstructorHelpers::FClassFinder<UUserWidget> PauseMenuBPClass(TEXT("/Game/data/menu_system/wbp_pause_menu"));

	/*
		Ha tal�l ilyen blueprint komponenst akkor adattagk�nt t�rolja azt 
	*/
	if (MainMenuBPClass.Class == nullptr) return;
	this->MainMenuClass = MainMenuBPClass.Class;

	if (PauseMenuBPClass.Class == nullptr) return;
	this->PauseMenuClass = PauseMenuBPClass.Class;
}

void UFPSGameInstance::Init()
{
	Super::Init();
}

/*
	A LoadMainMenu f�ggv�ny felel�s a f�men� bet�lt�s��rt
*/
void UFPSGameInstance::LoadMainMenu()
{
	if (MainMenuClass == nullptr) return;
	this->MainMenu = CreateWidget<UMenuWidget>(this, MainMenuClass);

	MainMenu->Setup();
	MainMenu->SetMenuInterface(this);					// Hozz�rendelni a men� interf�szhez saj�t mag�t
}

void UFPSGameInstance::LoadPauseMenu()
{
	if (MainMenuClass == nullptr) return;
	this->PauseMenu = CreateWidget<UMenuWidget>(this, PauseMenuClass);

	PauseMenu->Setup();
	PauseMenu->SetMenuInterface(this);
}

/*
	A Host f�ggv�ny felel�s egy szerver ind�t�s��rt
*/
void UFPSGameInstance::Host()
{
	/* Engine el�r�se az j�t�kbeli log haszn�lat�hoz */
	UEngine* Engine = GetEngine();
	if (Engine == nullptr) return;

	UWorld* World = GetWorld();
	if (World == nullptr) return;

	World->ServerTravel("/Game/data/maps/level_01?listen");
}

/*
	A Join f�ggv�ny felel�s egy szerver csatlakoztat�s��rt
*/
void UFPSGameInstance::Join(FString IPAddress)
{
	UEngine* Engine = GetEngine();
	if (Engine != nullptr)
		Engine->AddOnScreenDebugMessage(INDEX_NONE, 2.f, FColor::White, "Csatlakozas: " + IPAddress);

	APlayerController* PlayerController = GetFirstLocalPlayerController();
	if (PlayerController == nullptr) return;

	PlayerController->ClientTravel(IPAddress, ETravelType::TRAVEL_Absolute);
}

void UFPSGameInstance::QuitGame()
{
	APlayerController* PlayerController = GetFirstLocalPlayerController();
	if (PlayerController == nullptr) return;

	PlayerController->ConsoleCommand("quit");
}

void UFPSGameInstance::BackToTheGame()
{
	this->PauseMenu->TearDown();
}

void UFPSGameInstance::QuitToMainMenu()
{
	UWorld* World = GetWorld();
	if (World == nullptr) return;

	APlayerController* PlayerController = World->GetFirstPlayerController();
	if (PlayerController == nullptr) return;

	PlayerController->ClientTravel("/Game/data/menu_system/main_menu", ETravelType::TRAVEL_Absolute);
}
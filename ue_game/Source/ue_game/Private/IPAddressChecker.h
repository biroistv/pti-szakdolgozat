// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

class IPAddressChecker
{

public:
	IPAddressChecker() = default;
	~IPAddressChecker() = default;

	static bool IsValidIPAddress(const FString IPAddress);
};

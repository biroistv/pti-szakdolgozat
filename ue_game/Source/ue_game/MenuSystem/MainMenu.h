// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MenuSystem/MenuWidget.h"

#include "MainMenu.generated.h"

/**
 *
 */
UCLASS()
class UE_GAME_API UMainMenu : public UMenuWidget
{
	GENERATED_BODY()

public:
	/*
		Men� v�laszt� elem
	*/
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UWidgetSwitcher* MenuWidgetSwitcher;

	/*
		Men�k a MenuWidgetSwitcher-en bel�l
	*/
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UWidget* MainMenu;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UWidget* JoinMenu;

	/*
		F�men� elemei
	*/
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UButton* HostButton;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UButton* JoinButton;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UButton* ExitButton;

	/*
		Csatlakoz�s men� lemei
	*/
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UButton* CancelButton;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UButton* JoinIPButton;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UEditableTextBox* IPEditBox;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* JoinErrorMessage;

protected:
	virtual bool Initialize() override;

private:
	UFUNCTION()
		void HostServer();

	UFUNCTION()
		void QuitGame();

	UFUNCTION()
		void JoinGame();

	UFUNCTION()
		void OpenJoinMenu();

	UFUNCTION()
		void OpenMainMenu();

	UFUNCTION()
		bool IPValidation();
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "MenuInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UMenuInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * Ez az interf�sz tartalmazza a legt�bb j�t�kban defini�lt f�ggv�ny protot�pus�t.
 */
class UE_GAME_API IMenuInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/* A f�men�ben haszn�lt met�sudok*/
	virtual void Host() = 0;
	virtual void QuitGame() = 0;
	virtual void Join(FString IPAddress) = 0;

	/* A sz�net men�ben haszn�lt met�dusok*/
	virtual void BackToTheGame() = 0;
	virtual void QuitToMainMenu() = 0;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MenuSystem/MenuInterface.h"
#include "Engine/World.h"

#include "MenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class UE_GAME_API UMenuWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void SetMenuInterface(IMenuInterface*);

	/*
		UI fel�p�t�se ir�ny�t�s be�ll�t�s�val
	*/
	void Setup();

	/*
		UI le�p�t�se az ir�y�t�s be�ll�t�s�val
	*/
	void TearDown();

protected:
	virtual void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld) override;
	virtual bool Initialize() override;

	IMenuInterface* MenuInterfce;
	APlayerController* PlayerController;
};

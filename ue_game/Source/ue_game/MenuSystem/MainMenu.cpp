// Fill out your copyright notice in the Description page of Project Settings.

#include "MainMenu.h"

#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"
#include "Components/EditableTextBox.h"
#include "Components/TextBlock.h"

#include "Private/IPAddressChecker.h"


bool UMainMenu::Initialize()
{
	/* Ha az inicializ�l�s sikertelen akkor adja vissza a vez�rl�st a h�v� folyamatnak */
	bool bInitSuccess = Super::Initialize();
	if (!bInitSuccess) return false;

	/*
		Esem�nyek ind�t�sa a gombokra klikkel�s eset�n
	*/
	/* F�men� esem�nyei */
	if (this->HostButton == nullptr) return false;
	this->HostButton->OnClicked.AddDynamic(this, &UMainMenu::HostServer);

	if (this->JoinButton == nullptr) return false;
	this->JoinButton->OnClicked.AddDynamic(this, &UMainMenu::OpenJoinMenu);

	if (this->ExitButton == nullptr) return false;
	this->ExitButton->OnClicked.AddDynamic(this, &UMainMenu::QuitGame);

	/* Csatlkaoz�s men� esem�nyei*/
	if (this->CancelButton == nullptr) return false;
	this->CancelButton->OnClicked.AddDynamic(this, &UMainMenu::OpenMainMenu);

	if (this->JoinIPButton == nullptr) return false;
	this->JoinIPButton->OnClicked.AddDynamic(this, &UMainMenu::JoinGame);

	return true;
}

void UMainMenu::HostServer()
{
	if (this->MenuInterfce == nullptr) return;
	this->MenuInterfce->Host();
}

void UMainMenu::JoinGame()
{
	if (this->MenuInterfce == nullptr) return;
	if (this->IPEditBox == nullptr) return;
	if (!this->IPValidation()) return;

	this->MenuInterfce->Join(this->IPEditBox->GetText().ToString());
}

void UMainMenu::QuitGame()
{
	if (this->MenuInterfce == nullptr) return;
	this->MenuInterfce->QuitGame();
}

void UMainMenu::OpenJoinMenu()
{
	if (this->MenuWidgetSwitcher == nullptr) return;
	if (this->JoinMenu == nullptr) return;
	this->MenuWidgetSwitcher->SetActiveWidget(this->JoinMenu);
}

void UMainMenu::OpenMainMenu()
{
	// nullptr ellen�rz�sek
	if (this->MenuWidgetSwitcher == nullptr) return;
	if (this->MainMenu == nullptr) return;

	// Akt�v widget be�ll�t�sa
	this->MenuWidgetSwitcher->SetActiveWidget(this->MainMenu);
}

bool UMainMenu::IPValidation()
{
	FString IPString = IPEditBox->GetText().ToString();

	if (IPAddressChecker::IsValidIPAddress(IPString))
	{
		this->JoinErrorMessage->SetVisibility(ESlateVisibility::Hidden);
		return true;
	}
	else
	{
		this->JoinErrorMessage->SetVisibility(ESlateVisibility::Visible);

		if (IPString.IsEmpty())
		{
			this->JoinErrorMessage->SetText(FText::FromString("Nincs megadva IP cim"));
			return false;
		}
		else
		{
			this->JoinErrorMessage->SetText(FText::FromString("Hibas IP cim!"));
			return false;
		}
	}

	return false;
}

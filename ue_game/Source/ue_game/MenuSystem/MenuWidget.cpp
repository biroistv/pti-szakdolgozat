// Fill out your copyright notice in the Description page of Project Settings.

#include "MenuSystem/MenuWidget.h"

bool UMenuWidget::Initialize()
{
	bool bInitSuccess = Super::Initialize();
	if (!bInitSuccess) return false;

	return true;
}

void UMenuWidget::SetMenuInterface(IMenuInterface* MenuInterface)
{
	this->MenuInterfce = MenuInterface;
}

void UMenuWidget::OnLevelRemovedFromWorld(ULevel * InLevel, UWorld * InWorld)
{
	Super::OnLevelRemovedFromWorld(InLevel, InWorld);

	FInputModeGameOnly InputModeData;

	PlayerController->SetInputMode(InputModeData);
	PlayerController->bShowMouseCursor = false;
}

/*
	Ez a f�ggv�ny felel�s men�ben val� navig�l�s be�ll�t�s��rt.
*/
void UMenuWidget::Setup()
{
	if (this == nullptr) return;
	this->AddToViewport();

	/* PlayerController lek�r�se */
	this->PlayerController = GetWorld()->GetFirstPlayerController();
	if (PlayerController == nullptr) return;

	/* Beviteli m�d be�ll�t�sa */
	FInputModeUIOnly InputModeUIOnly;
	InputModeUIOnly.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);	// Ne r�gz�tse az eg�r poz�ci�j�t
	InputModeUIOnly.SetWidgetToFocus(this->TakeWidget());

	PlayerController->SetInputMode(InputModeUIOnly);	// �j beviteli m�d aktiv�l�sa
	PlayerController->bShowMouseCursor = true;			// Eg�r kurzor megjelen�t�se
}


void UMenuWidget::TearDown()
{
	this->RemoveFromViewport();

	UWorld* World = GetWorld();
	if (World == nullptr) return;

	this->PlayerController = GetWorld()->GetFirstPlayerController();
	if (PlayerController == nullptr) return;

	FInputModeGameOnly InputModeGameOnly;

	PlayerController->SetInputMode(InputModeGameOnly);
	PlayerController->bShowMouseCursor = false;
}

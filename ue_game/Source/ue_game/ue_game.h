// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

// Szerkesztőben elékszített konstansok
#define SURFACE_FLASHDEFAULT	SurfaceType1
#define SURFACE_FLASHVULNARABLE SurfaceType2
#define SURFACE_ROCK			SurfaceType3

#define COLLISION_WEAPON		ECC_GameTraceChannel1
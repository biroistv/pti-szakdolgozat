#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MenuSystem/MenuInterface.h"

#include "FPSGameInstance.generated.h"

/*
	Egyedi j�t�k p�ld�nyra vonatkoz� tulajdons�got tartalmaz� objektum.
*/
UCLASS()
class UE_GAME_API UFPSGameInstance : public UGameInstance, public IMenuInterface
{
	GENERATED_BODY()

public:
	UFPSGameInstance(const FObjectInitializer&);

	virtual void Init() override;

private:
	TSubclassOf<class UUserWidget> MainMenuClass;
	TSubclassOf<class UUserWidget> PauseMenuClass;

	class UMenuWidget* MainMenu;
	class UMenuWidget* PauseMenu;

	UFUNCTION(BlueprintCallable)
		void LoadMainMenu();

	UFUNCTION(BlueprintCallable)
		void LoadPauseMenu();

	UFUNCTION(exec)
		virtual void Host() override;

	UFUNCTION(exec)
		void Join(FString IPAddress);

	UFUNCTION(exec)
		virtual void QuitGame() override;

	UFUNCTION(exec)
		virtual void BackToTheGame() override;

	UFUNCTION(exec)
		virtual void QuitToMainMenu() override;
};

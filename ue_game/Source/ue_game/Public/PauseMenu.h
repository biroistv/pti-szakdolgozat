// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "MenuSystem/MenuWidget.h"

#include "Components/Button.h"

#include "PauseMenu.generated.h"

/**
 * 
 */
UCLASS()
class UE_GAME_API UPauseMenu : public UMenuWidget
{
	GENERATED_BODY()
	
	
public:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UButton* BackToGame;
	
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UButton* ExitToMainMenu;

protected:
	virtual bool Initialize() override;

private:
	UFUNCTION()
		void BackToTheGame();

	UFUNCTION()
		void QuitToMainMenu();
};

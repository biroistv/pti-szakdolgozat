// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FPSGame_GameMode.generated.h"

/**
 *
 */
UCLASS()
class UE_GAME_API AFPSGame_GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFPSGame_GameMode();

protected:
	virtual void Tick(float DeltaSeconds) override;

	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
		void RespawnPlayer();

	FTimerHandle TimeHandle;

	UPROPERTY(VisibleDefaultsOnly)
		float RespawnTimer = 15.f;
};

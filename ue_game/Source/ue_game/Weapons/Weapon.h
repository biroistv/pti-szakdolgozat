// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Weapon.generated.h"

// Infot t�rol a linetrace kezd� �s v�g pontj�t�l
USTRUCT()
struct FHitScanTrace
{
	GENERATED_BODY()

public:
	UPROPERTY()
		TEnumAsByte<EPhysicalSurface> SurfaceType;
	
	UPROPERTY()
		FVector_NetQuantize TraceEnd;
};

UCLASS()
class UE_GAME_API AWeapon : public AActor
{
	GENERATED_BODY()

public:
	AWeapon();

	//************************************
	// Met�dus:			StartFire
	// Teljes n�v:		AWeapon::StartFire
	// El�rhet�s�g:		public 
	// Visszat�r�s:		void
	// M�dis�t�:		-
	// Le�r�s:			L�v�s x m�sodpercenk�nt
	//************************************
	void StartFire();

	//************************************
	// Met�dus:			StopFire
	// Teljes n�v:		AWeapon::StopFire
	// El�rhet�s�g:		public 
	// Visszat�r�s:		void
	// M�dis�t�:		-
	// Le�r�s:			L�v�s befejez�se
	//************************************
	void StopFire();

protected:
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	//************************************
	// Met�dus:			Fire
	// Teljes n�v:		AWeapon::Fire
	// El�rhet�s�g:		protected 
	// Visszat�r�s:		void
	// M�dis�t�:		-
	// Le�r�s:			Ez a met�dus kezeli a t�zel�st a fegyverrel
	//************************************
	UFUNCTION(BlueprintCallable, Category = Weapon)
		void Fire();

	//************************************
	// Met�dus:			ServerFire
	// Teljes n�v:		AWeapon::ServerFire
	// El�rhet�s�g:		protected 
	// Visszat�r�s:		void
	// M�dost�:			-
	// Ler�s:			A szerver �lltal kezelt l�v�s elv�gz�se
	//************************************
	UFUNCTION(Server, Reliable, WithValidation)
		void ServerFire();

	/*
		Componens
	*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
		class USkeletalMeshComponent* FPSWeaponMesh = nullptr;

	/**/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
		TSubclassOf<class UDamageType> DamageType;

	/*
		Effektek
	*/
	UPROPERTY(EditDefaultsOnly, Category = Effects)
		class UParticleSystem* FireEffect = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
		class UParticleSystem* DefaultImpactEffect = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
		class UParticleSystem* FlashImpactEffect = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
		class UParticleSystem* BulletTraceEffect = nullptr;

	/*
		String azanis�t�k
	*/
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Effects)
		FName FireSocketname;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Effects)
		FName TracerTargetname;

	/**/
	UPROPERTY(EditDefaultsOnly, Category = Effects)
		TSubclassOf<class UCameraShake> CameraShakeClass;

	/*
		M�gikus sz�mok
	*/
	UPROPERTY(EditDefaultsOnly, Category = Weapon)
		float TraceLineLength = 10000.f;

	UPROPERTY(EditDefaultsOnly, Category = Weapon)
		float BaseDamage = 10.f;

	UPROPERTY(EditDefaultsOnly, Category = Weapon)
		float DamageMultiplier = 2.f;

	UPROPERTY(EditDefaultsOnly, Category = Weapon)
		float FireRate = 0.25f;

	/**/
	float LastFiredTime;

	FTimerHandle TimerManagerForAutomaticShot;

	UPROPERTY(ReplicatedUsing = OnRep_HitScanTrace)
	FHitScanTrace HitScanTrace;

	//************************************
	// Met�dus:			OnRep_HitScanTrace
	// Teljes n�v:		AWeapon::OnRep_HitScanTrace
	// El�rhet�s�g:		protected 
	// Visszat�r�s:		void
	// M�dost�:			-
	// Ler�s:			A replik�lt l�v�s eset�n megjelen�ti az effekteket minden kliens sz�m�ra
	//************************************
	UFUNCTION()
	void OnRep_HitScanTrace();

private:
	//************************************
	// Met�dus:			ShowMuzzleEffect
	// Teljes n�v:		AWeapon::ShowMuzzleEffect
	// El�rhet�s�g:		private 
	// Visszat�r�s:		void
	// M�dost�:			const
	// Param�terek:		FVector& - A traceline v�ge
	// Ler�s:			F�st efekt megjelent�se a cs� v�ge �s a becsap�d�s helye k�z�tt
	//************************************
	void ShowMuzzleEffect(FVector&) const;

	//************************************
	// Met�dus:			ShowFireEffect
	// Teljes n�v:		AWeapon::ShowFireEffect
	// El�rhet�s�g:		private 
	// Visszat�r�s:		void
	// M�dost�:			-
	// Ler�s:			A minden egyes t�zel�skor a fegyver cs� v�g�n t�z effeket jelenik meg
	//************************************
	void ShowFireEffect();

	//************************************
	// Met�dus:			ShowDefaultImpactEffect
	// Teljes n�v:		AWeapon::ShowDefaultImpactEffect
	// El�rhet�s�g:	    private 
	// Visszat�r�s:		void
	// M�dost�:			const
	// Param�terek:		FVector					- hol jelenjen meg az effekt
	//					class UParticleSystem*	- melyik effekt jelenjen meg
	// Ler�s:			Becsp�d�s effekt megjelen�t�se azon a helyen ahol a traceline v�get �r.
	//************************************
	void ShowImpactEffect(FVector, class UParticleSystem*) const;

	//************************************
	// Met�dus:			StartCameraShake
	// Teljes n�v:		AWeapon::StartCameraShake
	// El�rhet�s�g:		private 
	// Visszat�r�s:		void
	// M�dost�:			-
	// Ler�s:			A fegyver els�t�sekor a kicsi kamera r�zk�d�s be�ll�t�sa.
	//************************************
	void StartCameraShake();

	//************************************
	// Met�dus:			PlayInpactEffect
	// Teljes n�v:		AWeapon::PlayInpactEffect
	// El�rhet�s�g:	    private 
	// Visszat�r�s:		void
	// M�dost�:			const
	// Param�terek:		EPhysicalSurface	- fel�let t�pusa
	//					FVector				- hol jelenjen meg az effekt
	// Ler�s:			Ez a met�dus d�nti el hogy milyen effekt jelenjen meg a fel�lett�l f�gg�en
	//************************************
	void PlayInpactEffect(EPhysicalSurface, FVector) const;
};

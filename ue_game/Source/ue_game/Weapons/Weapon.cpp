// Fill out your copyright notice in the Description page of Project Settings.

#include "Weapon.h"
#include "ue_game.h"
#include "TimerManager.h"

#include "Components/SkeletalMeshComponent.h"

#include "Engine/World.h"

#include "DrawDebugHelpers.h"

#include "Kismet/GameplayStatics.h"

#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"

#include "GameFramework/DamageType.h"

#include "Camera/CameraShake.h"

#include "PhysicalMaterials/PhysicalMaterial.h"

#include "Net/UnrealNetwork.h"

#define OUT

// Debug line megjelen�t�se a j�t�kban el�rhet� parancs seg�ts�g�vel
static int32 DegubWeaponDrawing = 0;
FAutoConsoleVariableRef TraceLineDebug(
	TEXT("Multiplayer.DebugWeapons"),
	DegubWeaponDrawing,
	TEXT("Debug line drawing for weapons"),
	ECVF_Cheat
);

AWeapon::AWeapon()
{
	PrimaryActorTick.bCanEverTick = true;

	/*
		Componensek el�rhet�v� t�tele a blueprint szerkeszt�ben
	*/
	this->FPSWeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(FName{ "FPS Weapon modell" });
	if (this->FPSWeaponMesh == nullptr) return;

	this->FireSocketname = "FireSocket";
	this->TracerTargetname = "BeamEnd";

	/*
		Fegyver mesh be�ll�t�sia
	*/
	RootComponent = this->FPSWeaponMesh;
	this->FPSWeaponMesh->SetOnlyOwnerSee(true);
	this->FPSWeaponMesh->bCastDynamicShadow = false;
	this->FPSWeaponMesh->CastShadow = false;

	SetReplicates(true);

	NetUpdateFrequency = 66.f;
	MinNetUpdateFrequency = 33.f;
}

void AWeapon::BeginPlay()
{
	Super::BeginPlay();
}

void AWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(AWeapon, HitScanTrace, COND_SkipOwner);
}

void AWeapon::Fire()
{
	// Ha nem a szerver akar l�ni
	if (this->Role < ROLE_Authority)
	{
		this->ServerFire();
	}

	AActor* Actor = GetOwner();
	if (Actor == nullptr) return;

	/* A line trace kezd� piz�ci�j�t �s ir�ny�t t�rolj� voltoz�k */
	FVector EyeLocation{};
	FRotator EyeRotation{};

	/* Lek�ri, hogy honnan hova n�z a j�t�kos */
	Actor->GetActorEyesViewPoint(OUT EyeLocation, OUT EyeRotation);

	FVector ShotDirection = EyeRotation.Vector();

	/* A line trace hossza �s ir�nya */
	FVector TraceEnd = EyeLocation + (ShotDirection * this->TraceLineLength);
	FVector TracerEndPoint = TraceEnd;

	/* Egy�bb param�terek */
	FCollisionQueryParams Queryparams{};
	Queryparams.AddIgnoredActor(Actor);
	Queryparams.AddIgnoredActor(this);
	Queryparams.bTraceComplex = true;
	Queryparams.bReturnPhysicalMaterial = true;

	/* A Line trce �ltal eltal�l objektumok */
	FHitResult HitResult{};

	// Ha van tal�lat
	if (GetWorld()->LineTraceSingleByChannel(OUT HitResult, EyeLocation, TraceEnd, COLLISION_WEAPON, Queryparams))
	{
		AActor* HitActor = HitResult.GetActor();	// Melyik objektumot

		EPhysicalSurface PhysicalSurface = UPhysicalMaterial::DetermineSurfaceType(HitResult.PhysMaterial.Get());	// Milyen fel�lettel

		// Fel�let alap�n a sebz�s m�dos�t�sa
		float CalculatedDamage = this->BaseDamage;
		if (PhysicalSurface == SURFACE_FLASHVULNARABLE)
		{
			CalculatedDamage = this->BaseDamage * this->DamageMultiplier;
		}

		// Sebz�s alkalmaz�sa
		UGameplayStatics::ApplyPointDamage(
			HitActor,
			CalculatedDamage,
			ShotDirection,
			HitResult,
			Actor->GetInstigatorController(),
			this,
			DamageType);

		this->PlayInpactEffect(PhysicalSurface, HitResult.ImpactPoint);

		TracerEndPoint = HitResult.ImpactPoint;

		// Mikor l�tt utolj�ra
		this->LastFiredTime = GetWorld()->TimeSeconds;
	}

	if (DegubWeaponDrawing > 0)
	{
		/* Linetrace vonal vizu�lis megjelen�t�se debug c�lb�l */
		DrawDebugLine(GetWorld(), EyeLocation, TraceEnd, FColor::Cyan, false, 1.f, 0, 1.f);
	}


	if (this->Role == ROLE_Authority)
	{
		this->HitScanTrace.TraceEnd = TracerEndPoint;
	}

	/*
		Effektek megjelen�t�se
	*/
	this->ShowFireEffect();
	this->ShowMuzzleEffect(TracerEndPoint);
	this->StartCameraShake();
}

void AWeapon::OnRep_HitScanTrace()
{
	this->PlayInpactEffect(HitScanTrace.SurfaceType, HitScanTrace.TraceEnd);
	this->ShowFireEffect();
	this->ShowMuzzleEffect(HitScanTrace.TraceEnd);
	this->StartCameraShake();
}

void AWeapon::ServerFire_Implementation()
{
	this->Fire();
}

bool AWeapon::ServerFire_Validate()
{
	return true;
}

void AWeapon::ShowMuzzleEffect(FVector& TracerEndPoint) const
{
	FVector MuzzleLocation = FPSWeaponMesh->GetSocketLocation(this->FireSocketname);
	if (this->BulletTraceEffect == nullptr) return;

	/* Egy f�st effektet kezd a cs� v�g�r�l indtani */
	UParticleSystemComponent* TracerComp = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BulletTraceEffect, MuzzleLocation);
	if (TracerComp == nullptr) return;
	TracerComp->SetVectorParameter(this->TracerTargetname, TracerEndPoint);	// Telejesen a targetig
}

void AWeapon::ShowFireEffect()
{
	if (this->FireEffect == nullptr) return;
	UGameplayStatics::SpawnEmitterAttached(
		this->FireEffect,
		this->FPSWeaponMesh,
		this->FireSocketname);
}

void AWeapon::ShowImpactEffect(FVector ImpactPoint, UParticleSystem* Effect) const
{
	if (Effect == nullptr) return;
	UGameplayStatics::SpawnEmitterAtLocation(
		GetWorld(),
		Effect,
		ImpactPoint,
		ImpactPoint.Rotation());
}

void AWeapon::StartCameraShake()
{
	APawn* Owner = Cast<APawn>(GetOwner());
	if (Owner == nullptr) return;

	APlayerController* PlayerController = Cast<APlayerController>(Owner->GetController());
	if (PlayerController == nullptr) return;
	PlayerController->ClientPlayCameraShake(this->CameraShakeClass);
}

void AWeapon::PlayInpactEffect(EPhysicalSurface PhysicalSurface, FVector ImpactPoint) const
{
	switch (PhysicalSurface)
		{
		case SURFACE_FLASHDEFAULT:
		case SURFACE_FLASHVULNARABLE: {
			this->ShowImpactEffect(ImpactPoint, this->FlashImpactEffect);
			break;
		}
		default:
			this->ShowImpactEffect(ImpactPoint, this->DefaultImpactEffect);
			break;
		}
}

void AWeapon::StartFire()
{
	float TimeBetweenShots = FMath::Max(this->LastFiredTime + this->FireRate - GetWorld()->TimeSeconds, 0.f);

	GetWorldTimerManager().SetTimer(
		this->TimerManagerForAutomaticShot,
		this,
		&AWeapon::Fire,
		this->FireRate,
		true,
		TimeBetweenShots);
}

void AWeapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(this->TimerManagerForAutomaticShot);
}

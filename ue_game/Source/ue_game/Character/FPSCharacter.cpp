// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSCharacter.h"
#include "ue_game.h"

#include "Engine/World.h"

#include "Weapons/Weapon.h"

#include "Camera/CameraComponent.h"

#include "Components/InputComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "./Components/HealthComponent.h"
#include "Components/CapsuleComponent.h"

#include "GameFrameWork/SpringArmComponent.h"
#include "GameFramework/PawnMovementComponent.h"

#include "Net/UnrealNetwork.h"

AFPSCharacter::AFPSCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	this->WeaponSocketName = "WeaponSocket";

	/*
		Blueprint komponensek l�trehoz�sa
	*/
	this->FPSCharacterMesh = CreateDefaultSubobject<USkeletalMeshComponent>(FName{ "First Person Mesh" });
	if (this->FPSCharacterMesh == nullptr) return;

	this->FPSCameraComponent = CreateDefaultSubobject<UCameraComponent>(FName{ "FPS Camera" });
	if (this->FPSCameraComponent == nullptr) return;

	this->FPSHealthComponent = CreateDefaultSubobject<UHealthComponent>(FName{ "Player health" });
	if (this->FPSHealthComponent == nullptr) return;

	this->GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);

	/*
		Blueprint komponensek tulajdons�gainak be�ll�t�sa
	*/
	this->FPSCameraComponent->SetupAttachment(RootComponent);
	this->FPSCameraComponent->bUsePawnControlRotation = true;

	/* Az FPS n�zetnek */
	this->FPSCharacterMesh->SetOnlyOwnerSee(true);
	this->FPSCharacterMesh->SetupAttachment(FPSCameraComponent);
	this->FPSCharacterMesh->bCastDynamicShadow = false;
	this->FPSCharacterMesh->CastShadow = false;

	/* A teljes karakter elrejt�se a saj�t megunk el�l*/
	this->GetMesh()->SetOwnerNoSee(true);

	/* Gugol�s t�mogat�s lek�r�se */
	GetMovementComponent()->GetNavAgentPropertiesRef().bCanCrouch = true;
}

void AFPSCharacter::BeginPlay()
{
	Super::BeginPlay();

	this->FPSHealthComponent->OnHealthChange.AddDynamic(this, &AFPSCharacter::OnHealthChange);

	if (this->Role == ROLE_Authority)
	{
		// Alap�rtelmezett fegyver hozz�ad�sa az fps karakterhez
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		this->Weapon = GetWorld()->SpawnActor<AWeapon>(
			this->StarterWeaponClass,
			FVector::ZeroVector,
			FRotator::ZeroRotator,
			SpawnParameters);

		if (this->Weapon == nullptr)return;
		this->Weapon->SetOwner(this);

		this->Weapon->AttachToComponent(
			this->FPSCharacterMesh,
			FAttachmentTransformRules::SnapToTargetNotIncludingScale,
			this->WeaponSocketName);
	}
}

void AFPSCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AFPSCharacter, Weapon);
	DOREPLIFETIME(AFPSCharacter, bDied);
}

void AFPSCharacter::PossessedBy(AController* InController)
{
	Super::PossessedBy(InController);
}

// Called to bind functionality to input
void AFPSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	/* Billenyty�zettel val� mozg�s r�gz�t�se */
	PlayerInputComponent->BindAxis(FName{ "Move Forward" }, this, &AFPSCharacter::MoveForward);
	PlayerInputComponent->BindAxis(FName{ "Straight Right" }, this, &AFPSCharacter::MoveRight);

	/* Eg�rrel val� n�z�s */
	PlayerInputComponent->BindAxis(FName{ "Look" }, this, &AFPSCharacter::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(FName{ "Turn" }, this, &AFPSCharacter::AddControllerYawInput);

	/* J�t�kos mozg�s esem�nyek */
	PlayerInputComponent->BindAction(FName{ "Crouch" }, IE_Pressed, this, &AFPSCharacter::BeginCourch);
	PlayerInputComponent->BindAction(FName{ "Crouch" }, IE_Released, this, &AFPSCharacter::EndCrouch);
	PlayerInputComponent->BindAction(FName{ "Jump" }, IE_Pressed, this, &AFPSCharacter::BeginJump);

	/* Egy�bb esem�nyek */
	PlayerInputComponent->BindAction(FName{ "Fire" }, IE_Pressed, this, &AFPSCharacter::BeginFire);
	PlayerInputComponent->BindAction(FName{ "Fire" }, IE_Released, this, &AFPSCharacter::StopFire);

}

FVector AFPSCharacter::GetPawnViewLocation() const
{
	if (this->FPSCameraComponent == nullptr) return Super::GetPawnViewLocation();
	return FVector{ this->FPSCameraComponent->GetComponentLocation() };
}

void AFPSCharacter::MoveForward(float Value)
{
	AddMovementInput(GetActorForwardVector() * Value);
}

void AFPSCharacter::MoveRight(float Value)
{
	AddMovementInput(GetActorRightVector() * Value);
}

void AFPSCharacter::BeginCourch()
{
	Crouch();
}

void AFPSCharacter::EndCrouch()
{
	UnCrouch();
}

void AFPSCharacter::BeginJump()
{
	Jump();
}

void AFPSCharacter::BeginFire()
{
	if (this->Weapon == nullptr) return;
	this->Weapon->StartFire();
}

void AFPSCharacter::StopFire()
{
	if (this->Weapon == nullptr) return;
	this->Weapon->StopFire();
}

void AFPSCharacter::OnHealthChange(UHealthComponent* HealthComp, float Health, float HealtDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (Health <= 0.f && !this->bDied)
	{
		bDied = true;

		this->FPSCharacterMesh->SetOwnerNoSee(true);
		this->GetMesh()->SetOwnerNoSee(true);

		GetMovementComponent()->StopMovementImmediately();		// �lljon meg a mozg�sa
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);	// ne �tk�zz�n bele semmi

		DetachFromControllerPendingDestroy();

		SetLifeSpan(5.);
	}
}
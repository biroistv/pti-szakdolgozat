// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "FPSCharacter.generated.h"

UCLASS()
class UE_GAME_API AFPSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AFPSCharacter();

public:
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual FVector GetPawnViewLocation() const override;

protected:
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	/*
		Komponenesek
	*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
		class USkeletalMeshComponent* FPSCharacterMesh = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerCamera)
		class UCameraComponent* FPSCameraComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerHealth)
		class UHealthComponent* FPSHealthComponent = nullptr;

	UPROPERTY(Replicated)
		class AWeapon* Weapon = nullptr;

	/**/
	UPROPERTY(EditDefaultsOnly, Category = Player)
	TSubclassOf<class AWeapon> StarterWeaponClass;
	
	UPROPERTY(VisibleDefaultsOnly, Category = "Player")
	FName WeaponSocketName;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Player")
	bool bDied = false;

protected:
	virtual void PossessedBy(AController*) override;

	//************************************
	// Met�dus:			MoveForward
	// Teljes n�v:		AFPSCharacter::MoveForward
	// El�rhet�s�g:	    protected 
	// Visszat�r�s:		void
	// M�dost�:			-
	// Param�terek:		float - mozg�s ir�nya
	// Ler�s:			El�re vagy h�tra mozg�s aminek az ir�nya a param�ter negat�v vagy pozit�v �rt�k�t�l f�gg
	//************************************
	void MoveForward(float);

	//************************************
	// Met�dus:			MoveRight
	// Teljes n�v:		AFPSCharacter::MoveRight
	// El�rhet�s�g:	    protected 
	// Visszat�r�s:		void
	// M�dost�:			-
	// Param�terek:		float - mozg�s ir�nya
	// Ler�s:			Oldalaz� mozg�s aminek az ir�nya a param�ter negat�v vagy pozit�v �rt�k�t�l f�gg
	//************************************
	void MoveRight(float);

	//************************************
	// Met�dus:			BeginCourch
	// Teljes n�v:		AFPSCharacter::BeginCourch
	// El�rhet�s�g:	    protected 
	// Visszat�r�s:		void
	// M�dost�:			-
	// Ler�s:			A karakter guggoltat�sa
	//************************************
	void BeginCourch();

	//************************************
	// Met�dus:			EndCrouch
	// Teljes n�v:		AFPSCharacter::EndCrouch
	// El�rhet�s�g:	    protected 
	// Visszat�r�s:		void
	// M�dost�:			-
	// Ler�s:			Karaker fel�ll�t�sa guggol�sb�l
	//************************************
	void EndCrouch();

	//************************************
	// Met�dus:			BeginJump
	// Teljes n�v:		AFPSCharacter::BeginJump
	// El�rhet�s�g:	    protected 
	// Visszat�r�s:		void
	// M�dost�:			-
	// Ler�s:			Ez a met�dus hajtja v�gre az ugr�st
	//************************************
	void BeginJump();

	//************************************
	// Met�dus:			BeginFire
	// Teljes n�v:		AFPSCharacter::BeginFire
	// El�rhet�s�g:	    protected 
	// Visszat�r�s:		void
	// M�dost�:			-
	// Ler�s:			Megh�vja a fegyver azonos nev� f�ggv�ny�t
	//************************************
	void BeginFire();

	//************************************
	// Met�dus:			StopFire
	// Teljes n�v:		AFPSCharacter::StopFire
	// El�rhet�s�g:	    protected 
	// Visszat�r�s:		void
	// M�dost�:			-
	// Ler�s:			Megh�vja a fegyver azonos nev� f�ggv�ny�t
	//************************************
	void StopFire();

	//************************************
	// Met�dus:			OnHealthChange
	// Teljes n�v:		AFPSCharacter::OnHealthChange
	// El�rhet�s�g:	    protected 
	// Visszat�r�s:		void
	// M�dost�:			-
	// Param�terek:		UHealthComponent* HealthComp		- �let komponens
	//					float Health						- �let
	//					float HealtDelta				
	//					const class UDamageType* DamageType	- sebz�s t�pusa
	//					class AController* InstigatedBy		- ki szenvedi el
	//					AActor* DamageCauser				- ki sebezte
	// Ler�s:			Ez egy figyel� met�dus ami menden olyan esem�nyre reag�l ami az �let v�ltoz�s�val j�r
	//************************************
	UFUNCTION()
	void OnHealthChange(UHealthComponent* HealthComp, float Health, float HealtDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);
};

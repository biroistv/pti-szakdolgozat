// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "AngelStatue.generated.h"

/**
 * 
 */
UCLASS()
class UE_GAME_API AAngelStatue : public AStaticMeshActor
{
	GENERATED_BODY()

public:
	AAngelStatue();
	
protected:
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class UStaticMeshComponent* StaticMesh = nullptr;
	
	
};

// Fill out your copyright notice in the Description page of Project Settings.

#include "AngelStatue.h"
#include "ue_game.h"

#include "Components/StaticMeshComponent.h"

AAngelStatue::AAngelStatue()
{
	if (this->StaticMesh == nullptr) return;
	RootComponent = this->StaticMesh;

	this->StaticMesh->bDrawMeshCollisionIfComplex = true;
	this->StaticMesh->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	this->StaticMesh->SetCollisionObjectType(COLLISION_WEAPON);
}


